const coursesData = [
	{
		id: "wdc001",
		name: "PHP-Laravel",
		description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras blandit, orci eu rhoncus porta, turpis dui tempus lorem, in imperdiet quam velit sed urna. Ut viverra laoreet dolor, ultrices viverra sem tincidunt vitae. Duis ut varius lorem. Vivamus et nisl et sapien posuere convallis aliquet id turpis. Curabitur sodales erat sem, ullamcorper euismod est elementum sed. In ac erat arcu.",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python-Django",
		description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras blandit, orci eu rhoncus porta, turpis dui tempus lorem, in imperdiet quam velit sed urna. Ut viverra laoreet dolor, ultrices viverra sem tincidunt vitae. Duis ut varius lorem. Vivamus et nisl et sapien posuere convallis aliquet id turpis. Curabitur sodales erat sem, ullamcorper euismod est elementum sed. In ac erat arcu.",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java-Springboot",
		description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras blandit, orci eu rhoncus porta, turpis dui tempus lorem, in imperdiet quam velit sed urna. Ut viverra laoreet dolor, ultrices viverra sem tincidunt vitae. Duis ut varius lorem. Vivamus et nisl et sapien posuere convallis aliquet id turpis. Curabitur sodales erat sem, ullamcorper euismod est elementum sed. In ac erat arcu.",
		price: 45000,
		onOffer: true
	},
]

export default coursesData;