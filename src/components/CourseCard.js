import { Card, Button } from 'react-bootstrap';
import {useState, useEffect} from 'react';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}){
	console.log(courseProp);
	console.log(typeof courseProp);

	const { name, description, price, _id } = courseProp
 
/*	const [ count, setCount ] = useState(0);
	console.log(useState())


	const [ seats, setSeats ] = useState(10);
	const [ isOpen, setIsOpen ] = useState(true);

	function enroll(){
	        setCount(count + 1);
	        console.log(`Enrollees: ${count}`);
	        setSeats(seats - 1);
	        console.log(`Seats: ${seats}`);
	}

	useEffect(()=>{
		if(seats===0){
			setIsOpen(false);
		}
	},[seats]);

*/
	return(
			<Card>
				<Card.Body>
					<Card.Title>{name}</Card.Title>

					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text>{description}</Card.Text>

					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>{price}</Card.Text>

					<Link className = 'btn btn-primary' to={`/courses/${_id}`}>Details</Link>
				</Card.Body>
			</Card>
	)
}